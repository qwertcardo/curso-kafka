package com.qwertcardo.kafkacourse.constants;

import org.apache.kafka.common.serialization.StringSerializer;

public class Constants {
	//	PRODUCER CONSTANTS
	public final static String PRODUCER_BOOTSTRAP_SERVER = "127.0.0.1:9092";
	public final static String PRODUCER_KEY_SERIALIZER = StringSerializer.class.getName();
	public final static String PRODUCER_VALUE_SERIALIZER = StringSerializer.class.getName();
	public final static String PRODUCER_TOPIC = "java_topic_2";
	
	// CONSUMER CONSTANTS
}

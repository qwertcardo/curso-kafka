package com.qwertcardo.kafkacourse.tools;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.qwertcardo.kafkacourse.constants.Constants;

public class ProducerDemoApplication {
	
	public static void main(String[] args) {
		for (int i = 0; i < 250; i++) {
			ProducerDemoApplication.sendMessage(Integer.toString(i) + " Mensagem kafka via Java");
		}
	}
	
	private static Properties createProperties() {
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Constants.PRODUCER_BOOTSTRAP_SERVER);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, Constants.PRODUCER_KEY_SERIALIZER);
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, Constants.PRODUCER_VALUE_SERIALIZER);
		
		return properties;
	}
	
	private static KafkaProducer<String, String> createProducer() {
		return new KafkaProducer<String, String>(ProducerDemoApplication.createProperties());
	}
	
	private static ProducerRecord<String, String> createRecord(String message) {
		return new ProducerRecord<String, String>(Constants.PRODUCER_TOPIC, message);
	}
	
	private static void sendMessage(String message) {
		KafkaProducer<String, String> producer = ProducerDemoApplication.createProducer();
		producer.send(ProducerDemoApplication.createRecord(message));
		producer.flush();
		producer.close();
	}
	
}
